package com.x8academy;

import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class Terminal {

    private Directory currentDir;
    private TreeSet<Directory> childDirectoriesSorted;
    private TreeSet<File> filesSorted;

    public Terminal() {
        currentDir = new Directory("home");
    }

    public void openTerminal(Scanner scanner) {
        System.out.println("Terminal is Opened");
        System.out.println("Please enter command : ");

        while (scanner.hasNextLine()) {
            Set<Directory> childDirectories = currentDir.getChildDirectories();
            Set<File> filesInDirectory = currentDir.getFiles();

            String[] input = scanner.nextLine().split("\\s+");
            String command = input[0];

            if (command.equals("exit")) {
                System.out.println("Terminal will exit");
                break;
            }

            if (command.equals("ls") && input.length == 1) {
                Iterator<Directory> iteratorDirs = childDirectories.iterator();
                while (iteratorDirs.hasNext()) {
                    System.out.println(iteratorDirs.next());
                }

                Iterator<File> iteratorFiles = filesInDirectory.iterator();
                while (iteratorFiles.hasNext()) {
                    System.out.println(iteratorFiles.next());
                }
                continue;
            }

            if (input.length < 2) {
                System.out.println("Enter command and arguments");
                continue;
            }

            String firstArgument = input[1];

            switch (command) {
            case "cd":
                if (childDirectories.size() == 0) {
                    System.out.println("There is no such Directory");
                    continue;
                } else {
                    Directory tempDir = new Directory(firstArgument);
                    Iterator<Directory> iteratorDirs = childDirectories.iterator();
                    while (iteratorDirs.hasNext()) {
                        Directory childDirectory = iteratorDirs.next();
                        if (childDirectory.equals(tempDir)) {
                            currentDir = childDirectory;
                        }
                    }
                }
                break;
            case "mkdir":
                Directory tempDir = new Directory(firstArgument);
                if (childDirectories.contains(tempDir)) {
                    System.out.println(
                            "There is already existing directory named : " + firstArgument);
                    continue;
                } else {
                    currentDir.addChildDirs(tempDir);
                }
                break;
            case "createFile":
                File temp = new File(firstArgument);
                if (filesInDirectory.contains(temp)) {
                    System.out.println("There is already existing file named : " + firstArgument);
                    continue;
                } else {
                    filesInDirectory.add(new File(firstArgument));
                }
                break;
            case "cat":
                File file = getFileOrPrintError(filesInDirectory, firstArgument);
                if (file == null) {
                    System.out.println("There is no such file : " + firstArgument);
                } else {
                    file.readFile();
                }
                break;
            case "write":
                if (input.length < 4) {
                    System.out.println(
                            "Enter command Arguments : filename ; lineNumber ; lineContent");
                } else {
                    int lineNumber = Integer.parseInt(input[2]);
                    String lineContent = input[3];
                    File currentFile = getFileOrPrintError(filesInDirectory, firstArgument);
                    if (currentFile == null) {
                        System.out.println("There is no such file : " + firstArgument);
                    } else {
                        currentFile.writeIntoFile(lineNumber, lineContent);
                    }
                }

                break;
            case "ls":
                if (firstArgument.equals("--sortedDesc")) {
                    childDirectoriesSorted = new TreeSet<>(childDirectories);
                    filesSorted = new TreeSet<>(filesInDirectory);

                    Iterator<Directory> childDirectoriesSortedDescendingIterator = childDirectoriesSorted
                            .descendingIterator();
                    while (childDirectoriesSortedDescendingIterator.hasNext()) {
                        System.out.println(childDirectoriesSortedDescendingIterator.next());
                    }

                    Iterator<File> filesSortedDescendingOperator = filesSorted.descendingIterator();
                    while (filesSortedDescendingOperator.hasNext()) {
                        System.out.println(filesSortedDescendingOperator.next());
                    }
                }
                break;
            default:
                break;
            }
        }
    }

    private File getFileOrPrintError(Set<File> filesInDirectory, String firstArgument) {
        for (File file : filesInDirectory) {
            if (file.getName().equals(firstArgument)) {
                return file;
            }
        }
        return null;
    }
}
