package com.x8academy;

import java.util.HashSet;
import java.util.Set;

public class Directory implements Comparable<Directory> {

    private String name;
    private Set<Directory> childDirectories = new HashSet<>();
    private Set<File> files = new HashSet<>();

    public Directory(String name) {
        this.name = name;
    }

    public void addChildDirs(Directory dir) {
            childDirectories.add(dir);
    }

    public void saveFileToDirectory() {

    }

    public Set<File> getFiles() {
        return files;
    }


    public Set<Directory> getChildDirectories() {
        return childDirectories;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Directory other = (Directory) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int compareTo(Directory dir) {
        return this.toString().compareTo(dir.toString());
    }
}
