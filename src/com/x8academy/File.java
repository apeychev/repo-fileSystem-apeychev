package com.x8academy;

import java.util.HashMap;
import java.util.Map;

public class File implements Comparable<File> {

    private String name;
    private Map<Integer, String> content = new HashMap<>();
    private int maxLineNumber = -1;
    private int sizeOfFile = 0;

    public File(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getSizeOfFile() {
        return sizeOfFile;
    }

    public void writeIntoFile(int lineNumber, String lineContent) {
        content.put(lineNumber, lineContent);
        if (lineNumber > maxLineNumber) {
            maxLineNumber = lineNumber;
        }
    }

    public void readFile() {
        writeEmptyLines();

        for (Map.Entry<Integer, String> line : content.entrySet()) {
            System.out.println(line.getValue());
        }
    }

    private void writeEmptyLines() {
        int[] filledLines = new int[maxLineNumber + 1];
        for (Map.Entry<Integer, String> line : content.entrySet()) {
            filledLines[line.getKey()] = line.getValue().length();
            content.put(line.getKey(), line.getValue());
        }

        for (int i = 0; i < filledLines.length; i++) {
            if (filledLines[i] == 0) {
                content.put(i, "/n");
            } else {
                calculateSize(filledLines, i);
            }
        }
        sizeOfFile += filledLines.length;
    }

    private void calculateSize(int[] filledLinesWithSizes, int index) {
        sizeOfFile += filledLinesWithSizes[index];
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        File other = (File) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int compareTo(File file) {
        return this.toString().compareTo(file.toString());
    }
}
