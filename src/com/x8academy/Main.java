package com.x8academy;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Terminal terminalInstance = new Terminal();
        terminalInstance.openTerminal(new Scanner(System.in));
    }

}
